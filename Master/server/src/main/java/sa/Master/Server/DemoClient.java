package sa.Master.Server;

import sa.Master.common.Functions.LoggingUtilities;
import sa.Master.common.NetworkMessages.Login.LoginRequest;
import sa.Master.common.NetworkMessages.Login.LoginResponse;
import sa.Master.common.NetworkMessages.Validated.Request;
import sa.Master.common.NetworkMessages.Validated.Response;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class DemoClient implements Runnable {

    String jsonData = "{\"itemName\":\"newItem\", \"location\": \"everywhere\"}";
    String jsonData2 = "{\"itemName\":\"oldItem\", \"location\": \"nowhere\"}";
    String jsonData3 = "{\"itemName\":\"oldItem\", \"location\": \"somewhere\"}";

    private void demoRequest(Request request, Socket socket) {

        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Response response = null;
        try {
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());

            oos.writeObject(request);
            response = (Response) ois.readObject();

            LoggingUtilities.sysout(String.valueOf(response.success));
            LoggingUtilities.sysout((String) response.returnData.get("text"));
        } catch (Exception e) {
            LoggingUtilities.syserr(e.getMessage());
        }

    }

    public void run() {

        Socket socket = null;
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        try {
            socket = new Socket("10.250.48.100", 80);
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());

            oos.writeObject(new LoginRequest("Jason", "123456"));
            LoginResponse loginResponse = (LoginResponse) ois.readObject();
            if (loginResponse.status!=LoginResponse.ReturnStatus.success) {
                throw new Exception("Login Incorrect!");
            }

            // create new table newTable
            this.demoRequest(new Request.Builder("create")
                                     .setObject("table")
                                     .addDescription("table", "newTable")
                                     .addDescription("personal", "")
                                     .build(), socket);

            // create new item newItem in newTable
            this.demoRequest(new Request.Builder("create")
                                     .setObject("item")
                                     .addDescription("table", "newTable")
                                     .addDescription("item", "newItem")
                                     .addDescription("data", jsonData)
                                     .addDescription("personal", "")
                                     .build(), socket);

            // create new Item oldItem in newTable
            this.demoRequest(new Request.Builder("create")
                                     .setObject("item")
                                     .addDescription("table", "newTable")
                                     .addDescription("item", "oldItem")
                                     .addDescription("data", jsonData2)
                                     .addDescription("personal", "")
                                     .build(), socket);

            // show all Items in newTable
            this.demoRequest(new Request.Builder("show")
                                     .setObject("items")
                                     .addDescription("table", "newTable")
                                     .addDescription("personal", "")
                                     .build(), socket);

            // modify old item
            this.demoRequest(new Request.Builder("update")
                                     .setObject("item")
                                     .addDescription("table", "newTable")
                                     .addDescription("item", "oldItem")
                                     .addDescription("personal", "")
                                     .addDescription("data", jsonData3)
                                     .build(), socket);

            // show detail of oldItem in newTable
            this.demoRequest(new Request.Builder("show")
                                     .setObject("item")
                                     .addDescription("table", "newTable")
                                     .addDescription("item", "oldItem")
                                     .addDescription("personal", "")
                                     .build(), socket);

            oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(new Request.Builder("logout").addDescription("personal", "").build());
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try { oos.close(); ois.close(); socket.close(); } catch (IOException e) {}
        }

    }

}
