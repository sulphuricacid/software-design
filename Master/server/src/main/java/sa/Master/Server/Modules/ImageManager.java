package sa.Master.Server.Modules;

import sa.Master.Server.Server.Field;
import sa.Master.common.Functions.ImageUtilities;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.ImageGraphicAttribute;
import java.awt.image.BufferedImage;
import java.io.*;

public class ImageManager {

    private File serverRoot;

    public ImageManager(File serverRoot) {
        this.serverRoot = serverRoot;
    }

    /**
     * Get avatar image root's function
     *
     * @return File denoting the root
     * @throws FileNotFoundException on
     *      The avatar root directory don't exists
     */
    private File getAvatarRoot() throws FileNotFoundException {

        File avatarRoot = new File(this.serverRoot, "avatar");
        if (!(avatarRoot.isDirectory())) {
            throw new FileNotFoundException("Image manager: avatar directory not found.");
        }
        return avatarRoot;

    }

    /**
     * Get avatar image root's function
     *
     * @return File denoting the root
     * @throws FileNotFoundException on
     *      The avatar root directory don't exists
     */
    private File getTableImageRoot(Field field, String tableName) throws FileNotFoundException {

        File imageRoot = new File(serverRoot, (field==Field.personal?"personal":"group")+tableName);
        if (!(imageRoot.isDirectory())) {
            throw new FileNotFoundException(String.format("Image manager: table %s's image directory not found.", tableName));
        }
        return imageRoot;

    }

    private File getSpecificImage(File imageRoot, String fileName) throws FileNotFoundException, IOException {

        File imageFile = new File(imageRoot, fileName);

        if (!(imageFile.exists())) {
            throw new IOException(String.format("Image manager: %s: image not found.", imageFile.getAbsolutePath()));
        } else if (!(imageFile.canRead())) {
            throw new IOException(String.format("Image manager: %s: image not readable.", imageFile.getAbsolutePath()));
        }

        return imageFile;

    }

    /**
     * Get the avatar of a user
     *
     * @param userName user that owns the avatar
     * @return Avatar of the user, null if not found
     *
     * @throws IOException on
     *      Cannot read avatar file.
     */
    public byte[] showAvatar(String userName) throws IOException {

        byte[] avatar = null;

        File avatarRoot = getAvatarRoot();
        File avatarImage = getSpecificImage(avatarRoot, userName+".jpg");

        avatar = ImageUtilities.readImage(avatarImage);

        return avatar;

    }

    /**
     * Update a user's avatar
     */
    public void updateAvatar(String userName, byte[] newAvatar) throws IOException {

        File avatarRoot = getAvatarRoot();
        File avatarImage = getSpecificImage(avatarRoot, userName+".jpg");

        ImageUtilities.writeImage(avatarImage, newAvatar);

    }



    /**
     * Get the image of an item
     *
     * @param  field denotes whether the subject is a user or a group
     * @param  subjectName the name of the user or subject
     * @param  tableName the name of the table
     * @param  itemName the name of the item
     * @return Avatar of the user.
     *
     * @throws IOException on
     *      Item picture not exist.
     */
    public byte[] showItemImage(Field field, String subjectName, String tableName, String itemName) throws IOException {

        byte[] image = null;

        File imageRoot = getTableImageRoot(field, tableName);
        File imageFile = getSpecificImage(imageRoot, itemName+".jpg");

        image = ImageUtilities.readImage(imageFile);

        return image;

    }

    public void updateItemImage(Field field, String subjectName, String tableName, String itemName, byte[] newImage) throws IOException {

        File imageRoot = getTableImageRoot(field, tableName);
        File imageFile = new File(imageRoot, itemName+".jpg");

        ImageUtilities.writeImage(imageFile, newImage);

    }

}
