package sa.Master.Server.Modules;

import sa.Master.Server.Server;
import sa.Master.common.Functions.*;

/**
 * Defines the behavior when server gets Ctrl+C
 */
public class CtrlC_Handler implements Runnable {

    private Server server;

    public CtrlC_Handler(Server server) {
        this.server = server;
    }

    public void run() {

        try {
            server.persist();
            server.close();
        } catch (Exception e) {}

        LoggingUtilities.sysout("Server: shutting down.");

    }

}
