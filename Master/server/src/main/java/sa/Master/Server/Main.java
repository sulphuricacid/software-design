package sa.Master.Server;

import sa.Master.common.Functions.*;
import sa.Master.common.NetworkMessages.Login.*;
import sa.Master.common.NetworkMessages.Validated.*;

import org.apache.commons.cli.*;

import java.io.*;
import java.net.*;

/**
 * Hello world!
 *
 */
public class Main {

    public static void main(String[] args) throws Exception {

        CmdParser parser = null;
        try {
            parser = new CmdParser();
            parser.parse(args);

            Server server = new Server(
                    Integer.parseInt(parser.getField("p")),
                    new File(parser.getField("r")),
                    Integer.parseInt(parser.getField("m")),
                    Integer.parseInt(parser.getField("t")));
            // new Thread(new Client()).start();
            server.load();
            server.close();
        } catch (ParseException e) {
            parser.printHelp();
        }

    }

    static class CmdParser {

        private CommandLineParser parser;
        private Options options;
        private CommandLine cmdLine;

        public CmdParser() {

            options = new Options();
            options.addOption(Option.builder("r")
                                      .longOpt("root-directory")
                                      .hasArg()
                                      .valueSeparator()
                                      .desc("Specify root directory of server.")
                                      .required(true)
                                      .build()
                             );
            options.addOption(Option.builder("p")
                                      .longOpt("port-num")
                                      .hasArg()
                                      .valueSeparator()
                                      .desc("Specify the port number.")
                                      .required(true)
                                      .build()
                             );
            options.addOption(Option.builder("m")
                                      .longOpt("max-session")
                                      .hasArg()
                                      .valueSeparator()
                                      .desc("Specify maximum client number connects at a same time.")
                                      .required(true)
                                      .build()
                             );
            options.addOption(Option.builder("t")
                                      .longOpt("timeout")
                                      .hasArg()
                                      .valueSeparator()
                                      .desc("Specify timeout for each client.")
                                      .required(true)
                                      .build()
                             );
            parser = new DefaultParser();

        }

        public void parse(String[] args) throws ParseException {
            cmdLine = parser.parse(options, args);
        }

        public void printHelp() {

            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar <this-file> <args>", options);

        }

        public boolean exists(String field) {
            return cmdLine.hasOption(field);
        }

        public String getField(String field) {
            return cmdLine.getOptionValue(field);
        }

    }

}
