package sa.Master.common.Functions;

public class byteArray2HexString {

    /**
     * Convert byte array to heximal string
     * Used in password-hashing in loginRequest
     * @param bytes
     * @return converted string
    */
    public static String convert(final byte[] bytes) {
        StringBuilder builder = new StringBuilder();

        for (byte singleByte : bytes) {
            String tmp = Integer.toHexString(singleByte & 0xff);

            /*
             * A byte contains 2 hex digit.
             * Add 0 at high digit if the there's only 1 digit
             */
            if (tmp.length() == 1) {
                builder.append("0");
            }
            builder.append(tmp);
        }

        return builder.toString();
    }

}
