package sa.Master.common.Functions;

import com.google.gson.*;
import com.google.gson.stream.MalformedJsonException;

public class JsonFormat {

    public static String prettify(String originalJson) throws JsonParseException, MalformedJsonException {

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(originalJson);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(element);
        
    }

    public static String compact(String originalJson) throws JsonParseException, MalformedJsonException {

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(originalJson);
        Gson gson = new Gson();
        return gson.toJson(element);
        
    }
}
