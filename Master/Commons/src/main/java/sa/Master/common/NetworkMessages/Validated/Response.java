package sa.Master.common.NetworkMessages.Validated;

import java.io.Serializable;
import java.util.HashMap;

public class Response implements Serializable {
    
    private static final long serialVersionUID = 202L;

    public boolean success;
    public HashMap<String, Object> returnData;

    private Response(boolean success, HashMap data) {
        this.success = success;
        this.returnData = data;
    }

    public static class Builder {

        boolean success;
        HashMap<String, Object> data;

        public Builder(boolean success) {
            this.success = success;
            this.data = null;
        }

        public Builder addReturnData(String key, Object value) {

            if (this.data==null) {
                this.data = new HashMap<String, Object>();
            }
            this.data.put(key, value);

            return this;

        }

        public Response build() {
            return new Response(this.success, this.data);
        }

    }
    
}
