package sa.Master.common.Classes;

import java.util.ArrayList;

/**
 * User class contains all validation information of a user.
 *
 * @author whsu
 */
public class UserInfo {

    public String userName;
    public String passwordHashed;

    public long banDue;  // The time when this user can login again.
    public int wrongLoginTrial;  // After 5 wrong trials, the user will be banned.

    public ArrayList<String> joinedGroups;
    public ArrayList<String> adminGroups;

    public UserInfo() { }

    public UserInfo(String user, String psw, long banDue, ArrayList<String> groups, ArrayList<String> adminGroups) {
        this.userName = user;
        this.passwordHashed= psw;
        this.banDue = banDue;
        this.joinedGroups = groups;
        this.adminGroups = adminGroups;
    }

    /**
     * Copy only the userName, joinedGroups, adminGroups field.
     *
     * @param other UserInfo object to be copied.
     */
    public UserInfo(UserInfo other) {
        this.userName = other.userName;
        this.joinedGroups = other.joinedGroups;
        this.adminGroups = other.adminGroups;

        this.passwordHashed = "";
        this.banDue = 0;
        this.wrongLoginTrial = 0;
    }

}
