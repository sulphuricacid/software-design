package sa.Master.common.Classes;

/**
 * Client Exception
 * 
 * Throw when login failure in client.
 * 
 * @author whsu
 */
public class LoginFailureException extends java.lang.RuntimeException {

    public static final long serialVersionUID = 1000l;

    public LoginFailureException() {

    }

    public LoginFailureException(String msg) {
        super(msg);
    }

}
