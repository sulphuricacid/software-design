package sa.Master.common.NetworkMessages.Login;

import java.io.Serializable;

public class LoginResponse implements Serializable {
    
    private static final long serialVersionUID = 102l;

    /**
     * Login validation result.
     * 
     * This enum contains login results. Meanings are listed below.
     *      success: Login successful
     *      wrong_pwd: Wrong password
     *      blocked: User banned because of too many failed login attempts
     */
    public enum ReturnStatus {
        success,
        wrong_pwd,
        blocked
    }

    public ReturnStatus status;

    public LoginResponse(ReturnStatus s) {
        this.status = s;
    }

}
