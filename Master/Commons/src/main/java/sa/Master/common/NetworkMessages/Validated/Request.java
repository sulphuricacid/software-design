package sa.Master.common.NetworkMessages.Validated;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Request command syntax
 *  
 */
public class Request implements Serializable {

    private static final long serialVersionUID = 201l;

    public String action;
    public String object;
    public HashMap<String, Object> description;

    private Request(String action, String object, HashMap<String, Object> description) {
        this.action = action;
        this.object = object;
        this.description = description;
    }

    public static class Builder {
        
        private String action;
        private String object;
        private HashMap<String, Object> description;

        public Builder(String action) {
            this.action = action;
            this.description = new HashMap<String, Object>();
        }

        public Builder setObject(String object) {
            this.object = object;
            return this;
        }

        public Builder addDescription(String key, Object value) {
            this.description.put(key, value);
            return this;
        }

        public Request build() {
            return new Request(this.action, this.object, this.description);
        }

    }

}
