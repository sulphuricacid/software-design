package sa.Master.common.Classes;

public class SubjectNotFoundException extends java.lang.RuntimeException {

    public static final long serialVersionUID = 903l;

    public SubjectNotFoundException() {
        super();
    }

    public SubjectNotFoundException(String msg) {
        super(msg);
    }

}
