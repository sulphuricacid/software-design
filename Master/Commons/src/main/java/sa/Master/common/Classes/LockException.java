package sa.Master.common.Classes;

/**
 * Server Exception
 * 
 * Throw when modifying an object with a valid lock.
 * 
 * @author whsu
 */
public class LockException extends java.lang.RuntimeException {
    
    public static final long serialVersionUID = 900l;

    public LockException() {
        super();
    }

    public LockException(String msg) {
        super(msg);
    }

}
