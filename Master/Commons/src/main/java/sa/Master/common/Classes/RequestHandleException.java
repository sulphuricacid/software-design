package sa.Master.common.Classes;

/**
 * Server Exception
 * 
 * Throw when exception occurs in the process of request handling.
 * 
 * @author whsu
 */
public class RequestHandleException extends java.lang.RuntimeException {
    
    public static final long serialVersionUID = 901l;

    public RequestHandleException() {
        super();
    }

    public RequestHandleException(String msg) {
        super(msg);
    }

}
