package sa.Master.common.Functions;

import java.io.File;

public class removeDirectory {

    public static boolean remove(File directory) {

        if (directory.isDirectory()) {
            for (File child: directory.listFiles()) {
                if (child.isDirectory()) {
                    remove(child);
                } else {
                    child.delete();
                }
            }
            return directory.delete();
        }
        return !(directory.isDirectory());
        
    }
}
