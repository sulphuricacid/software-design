package sa.Master.common.NetworkMessages.Login;

import java.io.Serializable;

import sa.Master.common.Functions.*;

public class LoginRequest implements Serializable {

    private static final long serialVersionUID = 101l;
    
    public final String userName;
    public final String password;

    public LoginRequest(String userName, String passwordPlainText) {
        this.userName = userName;
        this.password = StringHash.hash(passwordPlainText);
    }

}


