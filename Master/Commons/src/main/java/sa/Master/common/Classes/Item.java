package sa.Master.common.Classes;

public class Item implements Comparable<Object> {
    
    public String itemName;
    public String location;
    public String lastTouched;

	@Override
	public int compareTo(Object other) {
		if (other instanceof Item) {
            return itemName.compareTo(((Item)other).itemName);
        } else {
            return 0;
        }
    }
    
}
