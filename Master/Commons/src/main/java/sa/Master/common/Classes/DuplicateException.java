package sa.Master.common.Classes;

public class DuplicateException extends RuntimeException {

    public static final long serialVersionUID = 903l;

    public DuplicateException() {
        super();
    }

    public DuplicateException(String msg) {
        super(msg);
    }

}
