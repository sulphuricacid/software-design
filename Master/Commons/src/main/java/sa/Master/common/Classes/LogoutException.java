package sa.Master.common.Classes;

/**
 * Server Exception
 * 
 * Throw when received logout request.
 */
public class LogoutException extends java.lang.RuntimeException {

    public static final long serialVersionUID = 902l;

    public LogoutException() {
    }

    public LogoutException(String msg) {
        super(msg);
    }

}
