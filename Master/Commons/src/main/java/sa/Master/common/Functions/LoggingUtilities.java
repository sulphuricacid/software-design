package sa.Master.common.Functions;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LoggingUtilities {

    public synchronized static void sysout(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS]");
        System.out.println(String.format("%s %s", dateFormat.format(new Date()), string));
    }

    public synchronized static void syserr(String string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss.SSS]");
        System.err.println(String.format("%s %s", dateFormat.format(new Date()), string));
    }

}
