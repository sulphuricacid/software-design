package sa.Master.common.Functions;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;

public class StringHash {

    public static final String HASH_METHOD = "SHA-256";

    /**
     * Hashes a string
     * Used in password-hashing in loginRequest
     * @param string String to be hashed
     * @return A heximal number string denoting the hash result
     */
    public static String hash(String string) {

        MessageDigest hashAlgorithm = null;
        String encodedString = null;

        try{
            hashAlgorithm = MessageDigest.getInstance(HASH_METHOD);
            hashAlgorithm.update(string.getBytes(StandardCharsets.UTF_8));
            encodedString = byteArray2HexString.convert(hashAlgorithm.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return encodedString;
    }

}
