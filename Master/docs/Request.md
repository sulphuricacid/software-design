# Request Structure Standards

## Overview

Request in this project (sa.Master.common.NetworkMessages.Validated.Request) consists of 3 fields: `action`, `object` and `description`.

The `action` is a `String` denoting which activity does the client wish the server to perform. Like to show, or to create **something**.

The `object` is a `String` describing the object(宾语) of the `action`. That is, to specify the **something** part.

The `description` is a `key-value pair` providing extra information for the `object`.

## Construct Template

```java
import sa.Master.common.NetworkMessages.Validated.Request;

class Test {
    public static void demo() {
        // build a new request
        Request request = new Request.Builder(/*Action Here*/)
                                     .setObject(/*Object Here*/)
                                     .addDescription(/*key1*/, /*value1*/)
                                     .addDescription(/*key2*/, /*value2*/) // ...
                                     .build();
    }
}
```

More specifically... If you want to get all the items' name in your `personal` table `Canteen`:

```java
import sa.Master.common.NetworkMessages.Validated.Request;

class Test {
    public static void demo2() {
        Request request = new Request.Builder("show")
                                     .setObject("items")
                                     .addDescription("personal", "")
                                     .addDescription("table", "Canteen") // ...
                                     .build();
    }
}
```

## All Possible Requests (Clustered by action)

### 1. The `show` request

Overview: Show an object. The object can be `tables`, `items`, `item`, and `userInfo`.

1. object=`tables`: Show **all tables' name** belonging to a **user**, or a **group**.
    
    **required**: Field Description [Note.1](#FieldDescription): key:`personal` or `group`, value: User's name or Group's name.

1. object=`items`: Show **all items' name** in a specific table.

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
1. object=`item`: Show **an item's detail (json and image)** information.

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
    **required**: Item Description: key:`item`, value: Item's name.

1. object=`userInfo`: Show a user's information. (Available since 1.0.5-nightly)

    **required**: Field Description: key:`personal`, value: User's name.



### 2. The `create` request

TODO create user

Overview: Create an object. The object can be `table`, `item`, and `group`.

1. object=`table`: Create a table for a **user**, or a **group** [Note.2](#CreateDeleteGroup).

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
1. object=`item`: Create an item in the specified table [Note.2](#CreateDeleteGroup).

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
    **required**: Item Description: key:`item`, value: Item's name.
    
    **required**: Item Detail Description: key:`data`, value: Item's detail information in json format.
    
    **optional**: Item image Description: key:`image`, value: Item's image of class `BufferedImage` converted to `byte[]`.

1. object=`group`: Create a group.
    
    **required**: Group name description: key:`group`, value: New Group's name.



### 3. The `update` request

Overview: Update an object's information. The object can be `item`, and `password`.

1. object=`item`: Update an item's information.

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
    **required**: Item Description: key:`item`, value: Item's name.
    
    **optional**: Item Detail Description json [Note.3](#JsonDetail): key:`data`, value: Item's detail information in json format.
    
    **optional**: Item image Description: key:`image`, value: Item's image of class `BufferedImage` converted to `byte[]`.

1. object=`password`: Update user's password.

    **required**: New Password Description: key:`newPassword`, value: User's new password.
    
    **required**: Old Password Description: key:`OldPassword`, value: User's old password.



### 4. The `delete` request

Overview: Delete an object. The object can be `table`, and `item`.

1. object=`table`: Delete a table belongs to a **User** or a **Group** [Note.2](#CreateDeleteGroup).

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.

1. object=`item`: Delete an item [Note.2](#CreateDeleteGroup).

    **required**: Field Description: key:`personal` or `group`, value: User's name or Group's name.
    
    **required**: Table Description: key:`table`, value: Table's name.
    
    **required**: Item Description: key:`item`, value: Item's name.



### 5. The `logout` request

Overview: Log out current session. No need for object or description.

## Notes

##### <a name="FieldDescription"></a> \[1\] FieldDescription
 Server will ignore the value of `person` description, implicitly interpret as current login user.
If both `person` and `group` exists, server will only read the `person` description. 

##### <a name="CreateDeleteGroup"></a> \[2\] CreateDeleteGroup
 TODO: Only **admin** of a Group can delete a table or an item belonging to that Group.

##### <a name="JsonDetail"></a> \[3\] JsonDetail
 See jsons.md for more information about json file's format.
