# Json Standards

## Overview

In the system, json files play a very important role in storing and transmitting files.
This document defines the structure of all json file's format.

## userData.json

### Overview

The `userData.json` is a **json array** containing objects of all users' validating information.

Members of user validating information object:

    1. userName: String
        Name of the user.
    2. passwordHashed: String
        Password of the user, hashed by SHA-256 algorithm, in heximal lower-case form.
    3. wrongTrials: integer
        Wrong login trials since last successful login.
    4. banDue: integer
        Time in unix timestamp(millisecond) when the user is allowed to login again.
    5. joinedGroups: Array of String
        Every String in this array is the group a user is in.
    6. adminGroups: Array of String
        Every String in this array is the group a user administrates.

### Example of a valid `userData.json`

```json
[
  {
    "userName": "Jason",
    "passwordHashed": "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92",
    "wrongTrials": 0,
    "banDue": 0,
    "joinedGroups": ["GuitarClub", "ProjectTeam"],
    "adminGroups": ["ProjectTeam"]
  },
  {
      "userName": "Anna",
      "passwordHashed": "54b688a517f7654563a6c64d945a3670880a4c602ec67a065bbebbcd2b22edd5",
      "wrongTrials": 5,
      "banDue": 1602840600000,
      "joinedGroups": ["GuitarClub", "ProjectTeam"],
      "adminGroups": ["GuitarClub"]
    }
]
```
### Statements of the above file:

The above file, contains 2 users: `Jason` and `Anna`. 

    `Jason`'s password is `123456`, while `Anna`'s is `123457` (Have a try)
    
    `Jason` didn't login until last successful login, while `Anna` had 5 trials.
    
    `Jason` can login after `0 (1970.1.1 8:00:00 +0800)`, while `Anna` is banned until `1602840600000 (2020.10.16 17:30:00 +0800)`
    
    Both `Jason` and `Anna` joined groups `GuitarClub` and `ProjectTeam`.
    
    While `Jason` administrates `ProjectTeam`, `Anna` administrates `GuitarClub`.
