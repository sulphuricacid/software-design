package top.whsu.FileServer.messages;

import java.io.Serializable;

public class ClientHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    // data field
    public String fileName;
    public String fileSuffix;
    public int fileSize;

    private ClientHeader(Builder builder) {

        this.fileName = builder.fileName;
        this.fileSuffix = builder.fileSuffix;
        this.fileSize = builder.fileSize;

    }

    public static class Builder {

        private int fileSize;
        private String fileName;
        private String fileSuffix;

        public Builder() { }

        public Builder setFileSize(int fileSize) {
            this.fileSize = fileSize;
            return this;
        }

        public Builder setFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }
        
        public Builder setFileSuffix(String fileSuffix) {
            this.fileSuffix = fileSuffix;
            return this;
        }

        public ClientHeader build() {
            return new ClientHeader(this);
        }
    }

}
