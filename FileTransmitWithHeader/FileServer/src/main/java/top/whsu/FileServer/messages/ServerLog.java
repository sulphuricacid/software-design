package top.whsu.FileServer.messages;

import java.io.Serializable;

public class ServerLog implements Serializable {
    
    public static final long serialVersionUID = 100L;

    // data field
    public int receivedSize;

    private ServerLog(Builder builder) {

        this.receivedSize = builder.receivedSize;

    }

    public static class Builder {

        private int receivedSize;

        public Builder() { }

        public Builder setReceivedSize(int receivedSize) {
            this.receivedSize = receivedSize;
            return this;
        }

        public ServerLog build() {
            return new ServerLog(this);
        }

    }

}
