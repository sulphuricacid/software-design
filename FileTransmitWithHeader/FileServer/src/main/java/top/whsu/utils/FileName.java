package top.whsu.utils;

import java.io.File;

public class FileName {
    
    public static String getFileName(File file) {
        String fileString = file.getName();

        if (fileString.isEmpty()) {
            return "";
        } else {
            int lastDotIndex = fileString.lastIndexOf('.');
            if (lastDotIndex==-1) {
                return fileString;
            } else {
                return fileString.substring(0, lastDotIndex);
            }
        }
    }

    public static String getExtension(File file) {
        String fileString = file.getName();

        if (fileString.isEmpty()) {
            return "";
        } else {
            int lastDotIndex = fileString.lastIndexOf('.');
            if (lastDotIndex==-1) {
                return "";
            } else {
                return fileString.substring(lastDotIndex+1);
            }
        }
    }

}
