package top.whsu.FileClient;

import java.io.*;
import java.net.*;
import top.whsu.messages.*;
import top.whsu.utils.*;

public class Client {

    private String serverAddress;
    private int portNum;

    private int fileSize;

    
    public Client(String serverAddress, int portNum) {
        this.serverAddress = serverAddress;
        this.portNum = portNum;
    }

    public void upload(String filePath) {
        File file = new File(filePath);
        this.upload(file);
    }

    public void upload(File file) {
        if (file.exists() && file.isFile()) {
            Socket socket = null;
            FileInputStream fin = null;
            try {
                fin = new FileInputStream(file);
                socket = new Socket(this.serverAddress, this.portNum);

                this.fileSize = fin.available();
                
                // send Header
                ClientHeader header = new ClientHeader.Builder()
                                                      .setFileName(FileName.getFileName(file))
                                                      .setFileSuffix(FileName.getExtension(file))
                                                      .setFileSize(this.fileSize)
                                                      .build();

                ObjectOutputStream headerSendStream =  new ObjectOutputStream(socket.getOutputStream());
                headerSendStream.writeObject(header);
                headerSendStream.flush();

                Logger logger = new Logger(socket.getInputStream());
                Uploader uploader = new Uploader(socket.getOutputStream(), fin);

                logger.getReady();

                Thread t1 = new Thread(logger);
                Thread t2 = new Thread(uploader);

                t1.start();
                t2.start();

                t2.join();
                t1.join();

                socket.close();
                fin.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            System.err.printf("%s: Not a file.\n", file.toPath());
        }
    }

    class Logger implements Runnable {

        private ObjectInputStream logStream;

        public Logger(InputStream inp) throws IOException {
            try{
                this.logStream = new ObjectInputStream(inp);
            } catch (IOException e) {
                System.err.printf("Abort due to Exception: %s\n%s\n", e.getClass(), e.getMessage());
                throw e;
            }
        }

        public boolean getReady() {
            ServerLog serverlog = null;
            try {
                serverlog = (ServerLog) this.logStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                System.err.printf("Abort due to Exception: %s\n%s\n", e.getClass(), e.getMessage());
            }
            if (serverlog.receivedSize==0) {
                return true;
            } else {
                return false;
            }
        }

        public void run() {
            int a = 0;
            int percentage = 0;
            ServerLog serverlog = null;
            while (true) {
                try {
                    serverlog = (ServerLog) this.logStream.readObject(); 
                } catch (IOException | ClassNotFoundException e) {
                    System.err.printf("Abort due to Exception: %s\n%s\n", e.getClass(), e.getMessage());
                    break;
                }

                a = serverlog.receivedSize;

                if (a==fileSize) {
                    System.out.println("Completed!");
                    break;
                } 
                else {
                    int nowPercentage = 100*a/fileSize;
                    if (percentage!=nowPercentage) {
                        percentage = nowPercentage;
                        System.out.printf("%d B of %d B transmitted, %f%%\n", a, fileSize, 100.0*a/fileSize);
                    }
                }
            }
        }
    }

    class Uploader implements Runnable {

        private OutputStream uploadStream;
        private FileInputStream fileInputStream;
        private byte[] buffer = new byte[1024];

        public Uploader(OutputStream uploadStream, FileInputStream fileInputStream) {
            this.uploadStream = uploadStream;
            this.fileInputStream = fileInputStream;
        }

        public void run() {
            int a = 0;
            try {
                while ((a=fileInputStream.read(buffer, 0, buffer.length)) != -1) {
                    uploadStream.write(buffer, 0, a);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
