package top.whsu.utils;

public class DataSizeFormat {

    private final static String[] suffix = new String[] {" B", " KiB", " MiB", " GiB"};

    public static String format(long byteCount) {
            int suffixIndex = 0;

            Long bc = byteCount;
            float num = bc.floatValue();
            while (num>=1030.0f && suffixIndex<3) {
                num /= 1024;
                suffixIndex++;
            }

            return num + suffix[suffixIndex];
    }    
}
