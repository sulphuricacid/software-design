package top.whsu.FileClient;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        Client client = new Client("127.0.0.1", 8079);
        client.upload(fileName);
        scanner.close();
    }
}
