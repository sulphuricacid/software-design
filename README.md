# 2020 HITsz Software Design A project: Property Management System - based on Java and Android.

COPYLEFT 2020-2020 **sa.Master** All rights reserved.

This repository contains **all** the source code of **Server** and **server-client communication** module of this system.

There\'s only pure java here.

For the Android work: Please see another member\'s work: [front-end-of-Property-Management-System](https://gitee.com/xion-cyber/stuff-manager)

